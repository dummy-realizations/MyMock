package taekwandodo.mock;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MockInvocationHandler implements InvocationHandler {
    private Method lastGivenMethod;
    private Object[] lastGivenArguments;
    private final List<MockedMethodData> returnObjectsData = new ArrayList<>();

    private Object findReturnValue(Method method, Object[] args) {
        for (MockedMethodData methodData : returnObjectsData) {
            if (methodData.getMethod().equals(method) && Arrays.deepEquals(methodData.getArgs(), args)) {
                return methodData.getReturnValue();
            }
        }

        return null;
    }

    public void saveReturnValue(Object returnValue) {
        returnObjectsData.add(new MockedMethodData(lastGivenMethod, lastGivenArguments, returnValue));
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Mock.setLastHandler(this);
        lastGivenMethod = method;
        lastGivenArguments = args;

        return findReturnValue(method, args);
    }
}
