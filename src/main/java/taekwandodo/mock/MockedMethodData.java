package taekwandodo.mock;

import java.lang.reflect.Method;

public class MockedMethodData {
    private final Method method;
    private final Object[] args;
    private final Object returnValue;

    public MockedMethodData(Method method, Object[] args, Object returnValue) {
        this.method = method;
        this.args = args;
        this.returnValue = returnValue;
    }

    public Method getMethod() {
        return method;
    }

    public Object[] getArgs() {
        return args;
    }

    public Object getReturnValue() {
        return returnValue;
    }
}