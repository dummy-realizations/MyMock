package taekwandodo.mock;

import java.lang.reflect.Proxy;

public class Mock {
    private static MockInvocationHandler lastHandler;

    @SuppressWarnings("unchecked")
    public static <T> T createMock(Class<T> classToBeMocked) {
        return (T) Proxy.newProxyInstance(
                classToBeMocked.getClassLoader(),
                new Class[] { classToBeMocked },
                new MockInvocationHandler()
        );
    }

    public static <T> ReturnValueSetter<T> setCondition(T mock) {
        return new ReturnValueSetter<>();
    }

    public static void setLastHandler(MockInvocationHandler handler) {
        lastHandler = handler;
    }

    public static class ReturnValueSetter<T> {
        public void setReturnValue(T returnValue) {
            lastHandler.saveReturnValue(returnValue);
        }
    }
}
