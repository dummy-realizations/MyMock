package taekwandodo;

public interface Animal {
    public String voice();

    public Integer getLegsCount();

    public Integer getPopulationSize(String species);

    public Integer getPopulationSize();
}
