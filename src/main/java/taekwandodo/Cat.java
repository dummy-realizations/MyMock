package taekwandodo;

public class Cat implements Animal {
    private String name;

    public Cat() {}

    public Cat(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String voice() {
        return "meow";
    }

    @Override
    public Integer getLegsCount() {
        return 4;
    }

    @Override
    public Integer getPopulationSize(String species) {
        return 100;
    }

    @Override
    public Integer getPopulationSize() {
        return 111;
    }
}
