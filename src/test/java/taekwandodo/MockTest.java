package taekwandodo;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import taekwandodo.mock.Mock;

public class MockTest {
    @Test
    public void catSpeechTest() {
        Cat cat = new Cat("Mursik");

        Animal mockCat = Mock.createMock(Animal.class);
        Mock.setCondition(mockCat.voice()).setReturnValue("meow");

        Assertions.assertEquals(cat.voice(), mockCat.voice());
    }

    @Test
    public void twoMocksTest() {
        Animal hare = Mock.createMock(Animal.class);
        Animal dog = Mock.createMock(Animal.class);

        Mock.setCondition(hare.voice()).setReturnValue("aaaaaa");
        Mock.setCondition(dog.voice()).setReturnValue("barking");

        Assertions.assertEquals(dog.voice(), "barking");
        Assertions.assertEquals(hare.voice(), "aaaaaa");
    }

    @Test
    public void twoMethodsTest() {
        Animal hare = Mock.createMock(Animal.class);

        Mock.setCondition(hare.voice()).setReturnValue("aaa");
        Mock.setCondition(hare.getLegsCount()).setReturnValue(4);

        Assertions.assertEquals(4, hare.getLegsCount());
        Assertions.assertEquals("aaa", hare.voice());
    }

    @Test
    public void twoSignaturesAndParamsTest() {
        Animal hare = Mock.createMock(Animal.class);

        Mock.setCondition(hare.getPopulationSize()).setReturnValue(10_000);
        Mock.setCondition(hare.getPopulationSize("white")).setReturnValue(6_000);
        Mock.setCondition(hare.getPopulationSize("black")).setReturnValue(4_000);

        Assertions.assertEquals(10_000, hare.getPopulationSize());
        Assertions.assertEquals(6_000, hare.getPopulationSize("white"));
        Assertions.assertEquals(4_000, hare.getPopulationSize("black"));
    }
}
